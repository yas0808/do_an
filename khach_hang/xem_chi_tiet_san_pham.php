<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Nhóm 08 BKC09</title>

    <!-- Icon css link -->
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/linearicons/style.css" rel="stylesheet">
    <link href="../vendors/flat-icon/flaticon.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Rev slider css -->
    <link href="../vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="../vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="../vendors/revolution/css/navigation.css" rel="stylesheet">
    <link href="../vendors/animate-css/animate.css" rel="stylesheet">
    
    <!-- Extra plugin css -->
    <link href="../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">
    
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    
    <!--================Main Header Area =================-->
    <header class="main_header_area">
     <div class="top_header_area row m0">
        <div class="container">
           <div class="float-left">
              <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
              <a href="mainto:haloteam888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> haloteam888@gmail.com</a>
          </div>
          <div class="float-right">
              <ul class="h_social list_style">
                 <?php 
                 if(isset($_SESSION['email_khach_hang'])){

                     echo '<li><a href="../dang_xuat.php">Đăng xuất</a></li>';

                 }
                 ?>
                 <li ><a href="khach_hang/chinh_sua_thong_tin_view.php" > Chỉnh sửa thông tin</a></li>
             </ul>
             <ul class="h_search list_style">
                 <li class="shop_cart"><a href="xem_gio_hang.php"><i class="lnr lnr-cart"></i></a></li>
                 
             </ul>
         </div>
     </div>
 </div>
 <div class="main_menu_area">
    <div class="container">
       <nav class="navbar navbar-expand-lg navbar-light bg-light">
          
       </a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="my_toggle_menu">
           <span></span>
           <span></span>
           <span></span>
       </span>
   </button>
   <div class="collapse navbar-collapse" id="navbarSupportedContent">
     <ul class="navbar-nav mr-auto">
        <li class="dropdown submenu active">
           <a class="dropdown-toggle" href="../index1.php" role="button">Trang Chủ</a>
           
       </li>
       <li class="dropdown submenu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Các sản phẩm</a>
        <ul class="dropdown-menu">
            <li><a href="mau_ve.php">Màu vẽ</a></li>
            <li><a href="but_ve.php">Bút vẽ</a></li>
            <li><a href="giay_ve.php">Giấy vẽ</a></li>
            <li><a href="artbook.php">Artbook</a></li>
            <li><a href="washi.php">Washi tape</a></li>
            <li><a href="bang_ve.php">Bảng vẽ điển tử</a></li>
        </ul>
    </li>
</div>
</nav>
</div>
</div>
</header>
<!--================End Main Header Area =================-->

<!--================Slider Area =================-->

<!--================End Slider Area =================-->

<!--================Welcome Area =================-->
<section class="welcome_bakery_area">
   <div class="container">
      
      <div class="cake_feature_inner">
         <div class="main_title">
          <h2>Xem sản phẩm</h2>
      </div>	
      <?php 
      require_once('../ket_noi.php');
      $ma_san_pham = $_GET['ma_san_pham'];
      $query="SELECT * FROM san_pham LEFT JOIN nha_san_xuat ON san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat LEFT JOIN loai_san_pham ON san_pham.ma_loai_san_pham = loai_san_pham.ma_loai_san_pham WHERE san_pham.ma_san_pham = $ma_san_pham";
      $result = mysqli_query($connect,$query);

      ?>
  </div>

  
  <div class="special_item_inner">
    <?php
    while($row = mysqli_fetch_array($result)){
        ?>
        
        <div class="specail_item">
            <div class="row">
                <div class="col-lg-4">
                </div>
                <div class="col-lg-8">
                    <div class="special_item_text">
                        <h4><?php echo $row['ten_san_pham'] ?></h4>
                        <p><?php echo $row['mo_ta'] ?></p>
                        <a class="pink_btn" href="../../khach_hang/them_vao_gio_hang.php"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="specail_item">
            <div class="row">
                <div class="col-lg-6">
                    <div class="s_item_left">
                        <div class="main_title">
                            <h2>Chi tiết sản phẩm</h2>
                        </div>
                        <ul class="list_style">
                            <li><a href="#">Tên sản phẩm: <?php echo $row['ten_san_pham']; ?></a></li>
                            <li><a href="#">Tên nhà sản xuất: <?php echo $row['ten_nha_san_xuat']; ?></a></li>
                            <li><a href="#">Tên loại sản phẩm: <?php echo $row['ten_loai_san_pham']; ?></a></li>
                            <li><a href="#">Giá: <?php echo $row['gia'];?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="s_right_img">
                        <img class="img-fluid" src="../admin/quan_ly_san_pham/anh/<?php echo $row['anh'] ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
</div>
</section>
<script src="....//js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="../../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="../../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<!-- Extra plugin js -->
<script src="../../vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="../../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
<script src="../../vendors/datetime-picker/js/moment.min.js"></script>
<script src="../../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../../vendors/nice-select/js/jquery.nice-select.min.js"></script>
<script src="../../vendors/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendors/lightbox/simpleLightbox.min.js"></script>

<script src="../../js/theme.js"></script>
</body>
</html>
</div>
</section>