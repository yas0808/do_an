
<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đổi mật khẩu</title>

        <!-- Icon css link -->
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../vendors/linearicons/style.css" rel="stylesheet">
        <link href="../vendors/flat-icon/flaticon.css" rel="stylesheet">
        <link href="../vendors/stroke-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="../vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="../vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="../vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="../vendors/animate-css/animate.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">
        
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!--================Main Header Area =================-->
        <header class="main_header_area">
            <div class="top_header_area row m0">
                <div class="container">
                    <div class="float-left">
                        <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                        <a href="mainto:haloteam888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> haloteam888@gmail.com</a>
                    </div>
                    <div class="float-right">
                        <ul class="h_social list_style">
                            <?php 
                                if(isset($_SESSION['email_khach_hang'])){

                            echo '<li><a href="dang_xuat.php">Đăng xuất</a></li>';

                            }
                        ?>
                            <li ><a href="chinh_sua_thong_tin_view.php" > Chỉnh sửa thông tin</a></li>
                            <li ><a href="doi_mat_khau_view.php" > Đổi mật khẩu</a></li>
                        </ul>
                        <ul class="h_search list_style">
                            <li class="shop_cart"><a href="xem_gio_hang.php"><i class="lnr lnr-cart"></i></a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="my_toggle_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="dropdown submenu active">
                                    <a class="dropdown-toggle" href="index.php" role="button">Trang Chủ</a>
                                </li>   
                                </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Các sản phẩm</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="mau_ve.php">Màu vẽ</a></li>
                                        <li><a href="but_ve.php">Bút vẽ</a></li>
                                        <li><a href="giay_ve.php">Giấy vẽ</a></li>
                                        <li><a href="artbook.php">Artbook</a></li>
                                        <li><a href="washi.php">Washi tape</a></li>
                                        <li><a href="bang_ve.php">Bảng vẽ điển tử</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="hoa_don_chua_duyet.php">Hóa đơn chưa duyệt</a></li>
                                        <li><a href="hoa_don_da_duyet.php">Hóa đơn đã duyệt</a></li>
                                        <li><a href="hoa_don_da_huy.php">Hóa đơn đã hủy</a></li>
                                    </ul>
                                </li>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
	<?php
		require_once('kiem_tra_khach_hang.php');
	?>
	<section class="contact_form_area p_100">
        	<div class="container">
        		<div class="main_title">
					<h2>Đổi mật khẩu</h2>
				</div>
                
       			<div class="row">
       				<div class="col-lg-7">
       					<form class="row contact_us_form" action="doi_mat_khau_process.php" >
							
							<div class="form-group col-md-6">
								<input type="password" class="form-control" name="mat_khau_cu" placeholder="Nhập mật khẩu cũ">
							</div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" name="mat_khau_moi" placeholder="Nhập mật khẩu mới">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" name="re_mat_khau_moi" placeholder="Nhập lại mật khẩu mới">
                            </div>
							<div class="form-group col-md-12">
								<button name="button_submit" value="1" class="btn order_s_btn form-control">Đổi mật khẩu</button>
							</div>
                            
						</form>
       				</div>
       				
       			</div>
        	</div>
        </section>
</body>
</html>