<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Xem giỏ hàng</title>

    <!-- Icon css link -->
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/linearicons/style.css" rel="stylesheet">
    <link href="../vendors/flat-icon/flaticon.css" rel="stylesheet">
    <link href="../vendors/stroke-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="../vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="../vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="../vendors/revolution/css/navigation.css" rel="stylesheet">
    <link href="../vendors/animate-css/animate.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">

    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <!--================Main Header Area =================-->
    <header class="main_header_area">
        <div class="top_header_area row m0">
            <div class="container">
                <div class="float-left">
                    <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                    <a href="mainto:haloteam888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> haloteam888@gmail.com</a>
                </div>
                <div class="float-right">
                    <ul class="h_social list_style">
                        <?php 
                        if(isset($_SESSION['email_khach_hang'])){

                            echo '<li><a href="dang_xuat_khach_hang.php">Đăng xuất</a></li>';

                        }
                        ?>
                        <?php 
                        if(isset($_SESSION['email_khach_hang'])){

                            echo '<li ><a href="chinh_sua_thong_tin_view.php" > Chỉnh sửa thông tin</a></li>';
                        }
                        ?>
                        <?php 
                        if(isset($_SESSION['email_khach_hang'])){

                            echo '<li ><a href="doi_mat_khau_view.php" > Đổi mật khẩu</a></li>';
                        }
                        ?>
                    </ul>
                    <ul class="h_search list_style">
                        <li class="shop_cart"><a href="xem_gio_hang.php"><i class="lnr lnr-cart"></i></a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="main_menu_area">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">

                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="my_toggle_menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="dropdown submenu active">
                            <a class="dropdown-toggle" href="../index.php" role="button">Trang Chủ</a>
                        </li>   
                    </li>
                    <li class="dropdown submenu">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Các sản phẩm</a>
                        <ul class="dropdown-menu">
                            <li><a href="../mau_ve.php">Màu vẽ</a></li>
                            <li><a href="../but_ve.php">Bút vẽ</a></li>
                            <li><a href="../giay_ve.php">Giấy vẽ</a></li>
                            <li><a href="../artbook.php">Artbook</a></li>
                            <li><a href="../washi.php">Washi tape</a></li>
                            <li><a href="../bang_ve.php">Bảng vẽ điển tử</a></li>
                        </ul>
                    </li>
                    <?php 
                    if(isset($_SESSION['email_khach_hang'])){
                        ?>
                        <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                            <ul class="dropdown-menu">
                                <li><a href="hoa_don_chua_duyet.php">Hóa đơn chưa duyệt</a></li>
                                <li><a href="hoa_don_da_duyet.php">Hóa đơn đã duyệt</a></li>
                                <li><a href="hoa_don_da_huy.php">Hóa đơn đã hủy</a></li>
                            </ul>
                        </li>
                        <?php 
                    }
                    ?>
                </div>
            </nav>
        </div>
    </div>
</header>
<section class="welcome_bakery_area">
    <div class="container">
        <div >
          <?php 

          if(!empty($_SESSION['gio_hang'])){
            $ma_khach_hang = $_SESSION['ma_khach_hang'];
            require_once('../ket_noi.php');
            $query  = "select * from khach_hang where ma_khach_hang = '$ma_khach_hang'";
            $result = mysqli_query($connect,$query);
            $row = mysqli_fetch_array($result);
            ?>
            <table border="1" width="100%">
                <tr>
                    <th>Tên Sản Phẩm</th>
                    <th>Ảnh</th>
                    <th>Giá</th>
                    <th>Số Lượng</th>
                    <th>Xóa</th>
                </tr>
                <?php 
                $tong = 0;
                foreach ($_SESSION['gio_hang'] as $ma_san_pham => $tung_san_pham) { 
                    ?>
                    <tr>
                        <td><?php echo $tung_san_pham['ten_san_pham'] ?></td>
                        <td><?php echo $tung_san_pham['anh'] ?></td>
                        <td><?php echo $tung_san_pham['gia'] ?></td>
                        <td>
                            <a href="thay_doi_so_luong_san_pham_trong_gio_hang.php?kieu=tru&ma_san_pham=<?php echo $ma_san_pham ?>">-</a>
                            <?php echo $tung_san_pham['so_luong'] ?>
                            <a href="thay_doi_so_luong_san_pham_trong_gio_hang.php?kieu=cong&ma_san_pham=<?php echo $ma_san_pham ?>">+</a>
                        </td>
                        <td>
                            <a href="xoa_san_pham_trong_gio_hang.php?ma_san_pham=<?php echo $ma_san_pham ?>">
                                Xóa
                            </a>
                        </td>
                    </tr>
                    <?php 
                    $tong += $tung_san_pham['gia']*$tung_san_pham['so_luong'];
                } 
                ?>
            </table>
            <h1>Tổng tiền phải tốn là: <?php echo $tong ?></h1>
            <a href="xoa_gio_hang.php">Xóa giỏ hàng</a>
            <h1>Thông tin người nhận</h1>
            <form action="dat_hang.php">
                Tên người nhận
                <input type="text" name="ten_khach_hang" value="<?php echo $row['ten_khach_hang'] ?>">
                <br>
                SDT người nhận
                <input type="text" name="sdt_khach_hang" value="<?php echo $row['sdt_khach_hang'] ?>">
                <br>
                Địa chỉ người nhận
                <textarea name="dia_chi_khach_hang"><?php echo $row['dia_chi_khach_hang'] ?></textarea>
                <br>
                <button name="button_submit" value="1">Đặt hàng</button>
            </form>
            <?php 
        }
        else{
            ?>
            <h1>Không có sản phẩm nào trong giỏ hàng</h1>
        <?php } ?>
        <a href="../index1.php">Xem tất cả sản phẩm</a>

    </div>
</div>
</section>
<script src="../js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<!-- Extra plugin js -->
<script src="../vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
<script src="../vendors/datetime-picker/js/moment.min.js"></script>
<script src="../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../vendors/nice-select/js/jquery.nice-select.min.js"></script>
<script src="../vendors/jquery-ui/jquery-ui.min.js"></script>
<script src="../vendors/lightbox/simpleLightbox.min.js"></script>

<script src="../js/theme.js"></script>
</body>
</html>