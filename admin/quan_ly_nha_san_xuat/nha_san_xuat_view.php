<?php $file_header_admin = "../dang_nhap_form.php"; 
require_once('../kiem_tra_admin.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trang chủ admin</title>

    <!-- Icon css link -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link htim_kiem./../vendors/linearicons/style.css" rel="stylesheet">
    <link href="../../vendors/flat-icon/flaticon.css" rel="stylesheet">
    <link href="../../vendors/stroke-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Rev slider css -->
    <link href="../../vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="../../vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="../../vendors/revolution/css/navigation.css" rel="stylesheet">
    <link href="../../vendors/animate-css/animate.css" rel="stylesheet">
    
    <!-- Extra plugin css -->
    <link href="../../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="../../vendor../s/magnifc-popup/magnific-popup.css" rel="stylesheet">
    
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!--================Main Header Area =================-->
    <header class="main_header_area">
        <div class="top_header_area row m0">
            <div class="container">
                <div class="float-left">
                    <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                    <a href="mainto:halo888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> halo888@gmail.com</a>
                </div>
                
                <div class="float-right">
                    <ul class="h_social list_style">

                       <li><a href="../update_mat_khau.php">Đổi mật khẩu</a></li>
                       <li><a href="../../dang_xuat.php">Đăng xuất</a></li>

                   </ul>
                   <ul class="h_search list_style">

                   </ul>
               </div>
           </div>
       </div>
       <div class="main_menu_area">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="my_toggle_menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="dropdown submenu active">
                            <a class="dropdown-toggle" href="../index1.php" role="button">Trang Chủ</a>
                        </li>
                        <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý khách hàng</a>
                            <ul class="dropdown-menu">
                                <li><a href="../quan_ly_khach_hang/view_khach_hang.php">Xem tất cả khách hàng</a></li>
                                
                            </ul>
                        </li>
                        
                        
                        <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý sản phẩm</a>
                            <ul class="dropdown-menu">
                                <li><a href="../quan_ly_san_pham/san_pham_view.php">Xem sản phẩm</a></li>
                                <li><a href="../quan_ly_san_pham/san_pham_insert_form.php">Thêm sản phẩm</a></li>
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                            <ul class="dropdown-menu">

                                <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php ">Hóa đơn chưa duyệt</a></li>
                                
                                <li><a href="../quan_ly_hoa_don/hoa_don_da_huy.php ">Hóa đơn đã hủy</a></li>
                                <li><a href="../quan_ly_hoa_don/hoa_don_da_duyet.php">Hóa đơn đã duyệt</a></li>
                                <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php">Hóa đơn chưa duyệt</a></li>
                            </ul>
                        </li>
                        <?php if($_SESSION['cap_do']==1){ ?>
                          <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhân viên</a>
                            <ul class="dropdown-menu">
                                <li><a href="../quan_ly_nhan_vien/nhan_vien_view.php">Tất cả nhân viên</a></li>
                                <li><a href="../quan_ly_nhan_vien/insert_form_nhan_vien.php">Thêm nhân viên</a></li>
                                
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                            <ul class="dropdown-menu">
                                <li><a href="nha_san_xuat_view.php">Xem nhà sản xuất</a></li>
                                <li><a href="nha_san_xuat_insert_form.php">Thêm nhà sản xuất</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
    </div>
</div>
</header>
<section class="contact_form_area p_100">
  <div class="container">
      <div class="main_title">
         <h2>Nhà sản xuất</h2>
     </div>
     <?php
     require_once('../../ket_noi.php');
        //mặc định lúc đầu không có giá trị tìm kiếm
     $tim_kiem = "";

        //nếu có thì truyền vào biến
     if(isset($_GET['tim_kiem'])){
        $tim_kiem = $_GET['tim_kiem'];
    }

        //giới hạn bản ghi trên 1 trang, ví dụ ở đây là limit: 1
    $limit = 7;

        //lúc đầu ở trang 1, không đc sửa
    $page = 1;

        //nếu chưa sang trang nào thì mặc định là 1, nếu không thì lấy về
    if (isset($_GET["page"])) { 
        $page  = $_GET["page"]; 
    }
    $offset = ($page-1) * $limit; 

        //câu lệnh lấy bản ghi
    $query_select = "select * from nha_san_xuat
    where ten_nha_san_xuat like '%$tim_kiem%' and ma_nha_san_xuat like '%$tim_kiem'
    limit $limit offset $offset";
    $result_select = mysqli_query($connect,$query_select);

        //lấy số bản ghi dựa theo tìm kiếm
    $query_count = "select count(*) from nha_san_xuat
    where ten_nha_san_xuat like '%$tim_kiem%'";
    $result_count = mysqli_query($connect,$query_count);
    $row_count = mysqli_fetch_array($result_count);
    $count = $row_count['count(*)'];

        //lấy số trang
    $total_page = ceil($count / $limit); 
    mysqli_close($connect);
    ?>
    <?php echo "<h2 align='center'>Có $count nhà sản xuất</h2>" ?>
    <table border="1" width="100%">
        <caption>
            <form>
                Tìm kiếm: 
                <input type="text" name="tim_kiem" value="<?php echo $tim_kiem ?>">
                <button>Tìm</button>
            </form>
        </caption>
        <tr align="center">
            <th>Mã NSX</th>
            <th >Tên nhà sản xuất</th>
            <th >Sửa</th>
        </tr>
        <?php
        while($row = mysqli_fetch_array($result_select)){
            ?>
            <tr>
                <td align="center"><?php echo $row['ma_nha_san_xuat'] ?></td>
                <td align="center"><?php echo $row['ten_nha_san_xuat'] ?></td>
                <td align="center">
                    <a href='edit_form_nha_san_xuat.php?ma_nha_san_xuat=<?php echo $row['ma_nha_san_xuat'] ?>'>Sửa
                    </a>
                </td>

                <tr>
                    <?php
                }
                ?>
            </table>
            <div align='center'>
                <?php
                for ($i=1; $i<=$total_page; $i++) 
                { 
                    ?> 
                    <a href='nha_san_xuat_view.php?page=<?php echo $i ?>&tim_kiem=<?php echo $tim_kiem ?>'><?php echo $i ?></a> 
                    <?php
                }
                ?>
            </div>
        </section>
        <script src="../../js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/popper.min.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="../../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="../../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <!-- Extra plugin js -->
        <script src="../../vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="../../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
        <script src="../../vendors/datetime-picker/js/moment.min.js"></script>
        <script src="../../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../../vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="../../vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="../../vendors/lightbox/simpleLightbox.min.js"></script>

        <script src="../../js/theme.js"></script>