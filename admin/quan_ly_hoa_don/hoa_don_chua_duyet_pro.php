<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
		$file_header_admin = "../dang_nhap_form.php"; 
		require_once('../kiem_tra_admin.php');
		require_once('../../ket_noi.php');
		$query = "select * from hoa_don
		join khach_hang
		on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang
		join hoa_don_chi_tiet
		on hoa_don.ma_hoa_don = hoa_don_chi_tiet.ma_hoa_don
		join san_pham
		on san_pham.ma_san_pham = hoa_don_chi_tiet.ma_san_pham
		where tinh_trang = 1
		order by thoi_gian_dat_hang";
		$result = mysqli_query($connect,$query);
		mysqli_close($connect);
		$array = array();
		while($row = mysqli_fetch_array($result)){
			$ma_hoa_don = $row['ma_hoa_don'];
			$array[$ma_hoa_don]['ten_khach_hang']     = $row['ten_khach_hang'];
			$array[$ma_hoa_don]['thoi_gian_dat_hang'] = $row['thoi_gian_dat_hang'];
			$array[$ma_hoa_don]['dia_chi']            = $row['dia_chi'];
			$array[$ma_hoa_don]['sdt_nguoi_nhan']                = $row['sdt_nguoi_nhan'];

			$ma_san_pham = $row['ma_san_pham'];
			$array[$ma_hoa_don]['san_pham'][$ma_san_pham]['so_luong']     = $row['so_luong'];
			$array[$ma_hoa_don]['san_pham'][$ma_san_pham]['ten_san_pham'] = $row['ten_san_pham'];
			$array[$ma_hoa_don]['san_pham'][$ma_san_pham]['gia']          = $row['gia'];
		}
		
	?>
	<table width="100%" border="1">
		<tr>
			<th>Ngày đặt hàng</th>
			<th>Thời gian đặt hàng</th>
			<th>Sản Phẩm</th>
			<th>Tên Khách Hàng</th>
			<th>Duyệt</th>
			<th>Hủy</th>
		</tr>
		<?php 
			foreach($array as $ma_hoa_don => $each_hoa_don){ 
			$tong_tien = 0;
		?>
			<tr>
				<td>
					<?php 
						$thoi_gian_dat_hang = $each_hoa_don['thoi_gian_dat_hang'];
						$thoi_gian_da_sua   = date("d-m", strtotime($thoi_gian_dat_hang));
						echo $thoi_gian_da_sua;
					?>
				</td>
				<td>
					<?php
						$thoi_gian_da_sua   = date("H:i", strtotime($thoi_gian_dat_hang));
						echo $thoi_gian_da_sua;
					?>
				</td>
				<td>
					<ul>
					<?php foreach ($each_hoa_don['san_pham'] as $each_san_pham) { ?>
						<li>
							<?php echo $each_san_pham['ten_san_pham'] ?> : <?php echo $each_san_pham['so_luong'] ?>
						</li>
					<?php 
						$tong_tien = $tong_tien + ($each_san_pham['gia']*$each_san_pham['so_luong']);
						} 
					?>
					</ul>
					<b>Tổng tiền là <?php echo $tong_tien ?></b>
				</td>
				<td>
					<?php echo $each_hoa_don['ten_khach_hang'] ?>
					<br>
					SDT:<?php echo $each_hoa_don['sdt_nguoi_nhan'] ?>
					<br>
					Địa chỉ:<?php echo $each_hoa_don['dia_chi'] ?>
				</td>
				<td>
					<a href="thay_doi_tinh_trang_hoa_don.php?ma_hoa_don=<?php echo $each_hoa_don['ma_hoa_don'] ?>&kieu=duyet">
						Duyệt
					</a>
				</td>
				<td>
					<a href="thay_doi_tinh_trang_hoa_don.php?ma_hoa_don=<?php echo $each_hoa_don['ma_hoa_don'] ?>&kieu=huy">
						Hủy
					</a>
				</td>
			</tr>
		<?php } ?>
	</table>
</body>
</html>