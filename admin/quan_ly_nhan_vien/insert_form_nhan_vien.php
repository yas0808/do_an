<?php require_once('../kiem_tra_sadmin.php');?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Thêm nhân viên</title>

        <!-- Icon css link -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet">
        <link href="../../vendors/linearicons/style.css" rel="stylesheet">
        <link href="../../vendors/flat-icon/flaticon.css" rel="stylesheet">
        <link href="../../vendors/stroke-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="../../vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="../../vendors/animate-css/animate.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="../../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="../../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">
        
        <link href="../../css/style.css" rel="stylesheet">
        <link href="../../css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!--================Main Header Area =================-->
        <header class="main_header_area">
            <div class="top_header_area row m0">
                <div class="container">
                    <div class="float-left">
                        <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                        <a href="mainto:halo888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> halo888@gmail.com</a>
                    </div>
                    
                    <div class="float-right">
                        <ul class="h_social list_style">
                            <li><a href="admin/dang_xuat.php">Đăng xuất</a></li>
                            <li><a href="../../update_mat_khau.php">Đổi mật khẩu</a></li>
                        </ul>
                        <ul class="h_search list_style">
                            <li class="shop_cart"><a href="#"><i class="lnr lnr-cart"></i></a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                       
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="my_toggle_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="dropdown submenu active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="trang_chu_admin.php" role="button" aria-haspopup="true" aria-expanded="false">Trang chủ</a>
                                </li>
                               
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý sản phẩm</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_san_pham/san_pham_view.php">Xem sản phẩm</a></li>
                                        <li><a href="../quan_ly_san_pham/san_pham_insert_form.php">Thêm sản phẩm</a></li>
                                    </ul>
                                    </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php ">Hóa đơn chưa duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_duyet.php">Hóa đơn đã duyệt </a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php">Hóa đơn chưa duyệt </a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_huy.php ">Hóa đơn đã hủy</a></li>
                                    </ul>
                                    </li>
                                    <?php if($_SESSION['cap_do']==1){ ?>
                                      <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhân viên</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="nhan_vien_view.php">Tất cả nhân viên</a></li>
                                        <li><a href="insert_form_nhan_vien.php">Thêm nhân viên</a></li>
                                        <li><a href="update_form_nhan_vien.php">Chỉnh sửa thông tin nhân viên</a></li>
                                    </ul>
                                    </li>
                                     <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_view.php">Xem nhà sản xuất</a></li>
                                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_insert_form.php">Thêm nhà sản xuất</a></li>
                                    </ul>
                                    </li>
                                    <?php } ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <?php
		
		require_once('../../ket_noi.php');
		$query = "select * from admin";
		$result = mysqli_query($connect,$query);
	?>
<section class="contact_form_area p_100">
        	<div class="container">
        		<div class="main_title">
					<h2>Thêm nhân viên</h2>
				</div>
                
       			<div class="row">
       				<div class="col-lg-7">
       					<form class="row contact_us_form" action="insert_process_nhan_vien.php" method="POST" enctype="multipart/form-data" >
							
							<div class="form-group col-md-6">
								<input  class="form-control" type="text" name="ten_admin" placeholder="Tên nhân viên">
							</div>
                            <div class="form-group col-md-6">
                                <input class="form-control" type="email" name="email_admin" placeholder="Email">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" name="mat_khau_admin" placeholder="Mật khẩu">
                            </div>
                            <div class="form-group col-md-6">
                                <input class="form-control" type="text" name="dia_chi_admin" placeholder="Đia chỉ">
                            </div>
                            <div class="form-group col-md-6">
                                <input class="form-control" type="text" name="sdt_admin" placeholder="Số điện thoại">
                            </div>

                            <div class="form-group col-md-6">
                            	Ngày sinh
                                <input class="form-control" type="date" name="ngay_sinh_admin" >
                            </div>
                            <div  class="form-group col-md-6">
                                <div class="form-radio form-radio-inline">
                                <div class="form-radio-legend">Giới tính</div>
                                    <label class="form-radio-label">
                                        <input name="gioi_tinh_admin" class="form-radio-field" type="radio" required checked value="1" />
                                        <i class="form-radio-button"></i>
                                        <span>Nam</span>
                                    </label>
                                    <label class="form-radio-label">
                                        <input name="gioi_tinh_admin" class="form-radio-field" type="radio" required value="0" />
                                        <i class="form-radio-button"></i>
                                        <span>Nữ</span>
                                    </label>
                                </div>
                            </div>
                            <div  class="form-group col-md-6">
                                <div class="form-radio form-radio-inline">
                                <div class="form-radio-legend">Cấp độ</div>
                                    <label class="form-radio-label">
                                        <input name="cap_do" class="form-radio-field" type="radio" required checked value="1" />
                                        <i class="form-radio-button"></i>
                                        <span>Super Admin</span>
                                    </label>
                                    <label class="form-radio-label">
                                        <input name="cap_do" class="form-radio-field" type="radio" required value="0" />
                                        <i class="form-radio-button"></i>
                                        <span>Admin</span>
                                    </label>
                                </div>
                            </div>
							<div class="form-group col-md-12">
								<button name="button_submit" value="1" class="btn order_s_btn form-control">Thêm nhân viên</button>
							</div>
                            
						</form>
       				</div>
       				
       			</div>
        	</div>
        </section>
        <script src="../../js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/popper.min.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="../../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="../../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <!-- Extra plugin js -->
        <script src="../../vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="../../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
        <script src="../../vendors/datetime-picker/js/moment.min.js"></script>
        <script src="../../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../../vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="../../vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="../../vendors/lightbox/simpleLightbox.min.js"></script>
        
        <script src="../../js/theme.js"></script>
</body>
</html>