<?php require_once('../kiem_tra_sadmin.php');?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Thêm nhân viên</title>

  <!-- Icon css link -->
  <link href="../../css/font-awesome.min.css" rel="stylesheet">
  <link href="../../vendors/linearicons/style.css" rel="stylesheet">
  <link href="../../vendors/flat-icon/flaticon.css" rel="stylesheet">
  <link href="../../vendors/stroke-icon/style.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="../../css/bootstrap.min.css" rel="stylesheet">

  <!-- Rev slider css -->
  <link href="../../vendors/revolution/css/settings.css" rel="stylesheet">
  <link href="../../vendors/revolution/css/layers.css" rel="stylesheet">
  <link href="../../vendors/revolution/css/navigation.css" rel="stylesheet">
  <link href="../../vendors/animate-css/animate.css" rel="stylesheet">

  <!-- Extra plugin css -->
  <link href="../../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
  <link href="../../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">

  <link href="../../css/style.css" rel="stylesheet">
  <link href="../../css/responsive.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>

      <!--================Main Header Area =================-->
      <header class="main_header_area">
        <div class="top_header_area row m0">
          <div class="container">
            <div class="float-left">
              <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
              <a href="mainto:halo888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> halo888@gmail.com</a>
            </div>

            <div class="float-right">
              <ul class="h_social list_style">
                <li><a href="../dang_xuat.php">Đăng xuất</a></li>
                <li><a href="../update_mat_khau.php">Đổi mật khẩu</a></li>
              </ul>
              <ul class="h_search list_style">


              </ul>
            </div>
          </div>
        </div>
        <div class="main_menu_area">
          <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="my_toggle_menu">
                  <span></span>
                  <span></span>
                  <span></span>
                </span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="dropdown submenu active">
                    <a class="dropdown-toggle" href="../index1.php" role="button">Trang chủ</a>
                  </li>

                  <li class="dropdown submenu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý sản phẩm</a>
                    <ul class="dropdown-menu">
                      <li><a href="../quan_ly_san_pham/san_pham_view.php">Xem sản phẩm</a></li>
                      <li><a href="../quan_ly_san_pham/san_pham_insert_form.php">Thêm sản phẩm</a></li>
                    </ul>
                  </li>

                  <li class="dropdown submenu">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý khách hàng</a>
                                <ul class="dropdown-menu">
                                    <li><a href="../quan_ly_khach_hang/view_khach_hang.php">Xem khách hàng</a></li>
                                </ul>
                            </li> 
                  <li class="dropdown submenu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                    <ul class="dropdown-menu">
                      <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php ">Hóa đơn chưa duyệt</a></li>
                      <li><a href="../quan_ly_hoa_don/hoa_don_da_duyet.php">Hóa đơn đã duyệt </a></li>

                      <li><a href="../quan_ly_hoa_don/hoa_don_da_huy.php ">Hóa đơn đã hủy</a></li>
                    </ul>
                  </li>
                  <?php if($_SESSION['cap_do']==1){ ?>
                    <li class="dropdown submenu">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhân viên</a>
                      <ul class="dropdown-menu">
                        <li><a href="nhan_vien_view.php">Tất cả nhân viên</a></li>
                        <li><a href="insert_form_nhan_vien.php">Thêm nhân viên</a></li>
                        
                      </ul>
                    </li>
                    <li class="dropdown submenu">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                      <ul class="dropdown-menu">
                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_view.php">Xem nhà sản xuất</a></li>
                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_insert_form.php">Thêm nhà sản xuất</a></li>
                      </ul>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </header>
      <?php

      require_once('../../ket_noi.php');
      $tim_kiem = "";
      if(isset($_GET['tim_kiem'])){
        $tim_kiem = $_GET['tim_kiem'];
      }

        //vào trang đầu tiên
      $trang = 1;
      if(isset($_GET['trang'])){
        $trang = $_GET['trang'];
      }
        //số sp hiển thị trên 1 trang
      $limit = 4;

        //số sản phẩm bỏ qua
      $offset = ($trang-1)*$limit;
      $query = "select * from admin";
      $result = mysqli_query($connect,$query);
      $query_count  = "select count(*) from admin
      where ten_admin like '%$tim_kiem%'";
      $result_count = mysqli_query($connect,$query_count);
      $row_count    = mysqli_fetch_array($result_count);
      $count        = $row_count['count(*)'];

        //số trang
      $so_trang = ceil($count/$limit);

      mysqli_close($connect);
      ?>
      <section class="contact_form_area p_100">
       <div class="container">
        <div class="main_title">
         <h2>Xem nhân viên</h2>
       </div>
        <div align="center">
                <caption>
                    <form>
                        Tìm kiếm: 
                        <input type="text" name="tim_kiem" value="<?php echo $tim_kiem ?>">
                        <button>Tìm</button>
                    </form>
                </caption>
            </div>
       <table border="1" width="100%">
        <tr>
         <th>Mã </th>
         <th>Tên </th>
         <th>Email</th>
         <th>Địa chỉ</th>
         <th>SĐT</th>
         <th>Ngày sinh</th>
         <th>Giới tính</th>
         <th>Cấp độ</th>
         <th>Sửa</th>
         <th>Xóa</th>
       </tr>
       <?php
       while ($row = mysqli_fetch_array($result)) {
        ?>
        <tr>
          <td><?php echo $row['ma_admin'] ?></td>
          <td><?php echo $row['ten_admin'] ?></td>
          <td><?php echo $row['email_admin'] ?></td>
          <td><?php echo $row['dia_chi_admin'] ?></td>
          <td><?php echo $row['sdt_admin'] ?></td>
          <td><?php echo $row['ngay_sinh_admin'] ?></td>
          <td>
           <?php
           if($row['gioi_tinh_admin']==1){
            echo "Nam";
          }
          else{
           echo "Nữ";
         }
         ?>
       </td>
       <td>
         <?php
         if($row['cap_do']==1){
          echo "Super Admin";
        }
        else{
         echo "Admin";
       }
       ?>
     </td>
     <td>
       <a href='update_form_nhan_vien.php?ma_admin=<?php echo $row['ma_admin'] ?>'>Sửa
       </a>
     </td>
     <td>
       <a href='delete_nhan_vien.php?ma_admin=<?php echo $row['ma_admin'] ?>'>Xóa
       </a>
     </td>
     <?php
   }
   ?>
 </tr>
</table>
<br>

<a href="insert_form_nhan_vien.php"><button>Thêm nhân viên</button></a>

</div>
<div align="center">
                <?php for($i=1; $i<=$so_trang; $i++){ ?>
                    <a href="?trang=<?php echo $i ?>&tim_kiem=<?php echo $tim_kiem ?>"><?php echo $i ?></a>
                <?php } ?>
                
            </div>
</section>
</section>
<script src="../../js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="../../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="../../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="../../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<!-- Extra plugin js -->
<script src="../../vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="../../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
<script src="../../vendors/datetime-picker/js/moment.min.js"></script>
<script src="../../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../../vendors/nice-select/js/jquery.nice-select.min.js"></script>
<script src="../../vendors/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendors/lightbox/simpleLightbox.min.js"></script>

<script src="../../js/theme.js"></script>
</body>
</html>