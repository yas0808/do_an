<?php $file_header_admin = "../dang_nhap_form.php"; 
		require_once('../kiem_tra_admin.php'); ?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trang chủ admin</title>

        <!-- Icon css link -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet">
        <link htim_kiem./../vendors/linearicons/style.css" rel="stylesheet">
        <link href="../../vendors/flat-icon/flaticon.css" rel="stylesheet">
        <link href="../../vendors/stroke-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="../../vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="../../vendors/animate-css/animate.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="../../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="../../vendor../s/magnifc-popup/magnific-popup.css" rel="stylesheet">
        
        <link href="../../css/style.css" rel="stylesheet">
        <link href="../../css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
       <body>
        <!--================Main Header Area =================-->
        <header class="main_header_area">
            <div class="top_header_area row m0">
                <div class="container">
                    <div class="float-left">
                        <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                        <a href="mainto:halo888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> halo888@gmail.com</a>
                    </div>
                    
                    <div class="float-right">
                        <ul class="h_social list_style">
                            
                           
                            <li><a href="../../dang_xuat.php">Đăng xuất</a></li>
                            
                        </ul>
                        <ul class="h_search list_style">
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                       
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="my_toggle_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="dropdown submenu active">
                                    <a class="dropdown-toggle" href="../index1.php" role="button">Trang Chủ</a>
                                </li>
                               
                                <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý khách hàng</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="khach_hang_view.php">Xem tất cả khách hàng</a></li>
                                        <li><a href="khach_hang_insert_form.php">Thêm khách hàng</a></li>
                                    </ul>
                                    </li>
        
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý sản phẩm</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_san_pham/san_pham_view.php">Xem sản phẩm</a></li>
                                        <li><a href="../quan_ly_san_pham/san_pham_insert_form.php">Thêm sản phẩm</a></li>
                                    </ul>
                                    </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                                    <ul class="dropdown-menu">

                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php ">Hóa đơn chưa duyệt</a></li>
                                       
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_huy.php ">Hóa đơn đã hủy</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_duyet.php">Hóa đơn đã duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php">Hóa đơn chưa duyệt</a></li>
                                    </ul>
                                    </li>
                                    <?php if($_SESSION['cap_do']==1){ ?>
                                      <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhân viên</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nhan_vien/nhan_vien_view.php">Tất cả nhân viên</a></li>
                                        <li><a href="../quan_ly_nhan_vien/insert_form_nhan_vien.php">Thêm nhân viên</a></li>
                                        
                                    </ul>
                                    </li>
                                     <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_view.php">Xem nhà sản xuất</a></li>
                                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_insert_form.php">Thêm nhà sản xuất</a></li>
                                    </ul>
                                    </li>
                                    <?php } ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <section class="contact_form_area p_100">
        <div class="container">
                <div class="main_title">
                    <h2>Sửa khách hàng</h2>
                </div>
                <?php 
        require_once('../../ket_noi.php');
        $ma_khach_hang = $_GET['ma_khach_hang'];
        $query = "select * from khach_hang where ma_khach_hang = '$ma_khach_hang'";
        $result = mysqli_query($connect,$query);
        $row = mysqli_fetch_array($result);
        mysqli_close($connect);
    ?>
    <div class="row">
                    <div class="col-lg-7">
                        <form class="row contact_us_form" action="update_process_khach_hang.php" >
                            
                            <div class="form-group col-md-12">
                                Tên khách hàng:
                                <input type="text" name="ten_khach_hang" value="<?php echo $row['ten_khach_hang'] ?>">
                            </div>
                             <div class="form-group col-md-12">
                                 Email:
                                <input type="text" name="email_khach_hang" value="<?php echo $row['email_khach_hang'] ?>">
                            </div>
                            
                            <div class="form-group col-md-12">
                                 Mật Khẩu:
                                <input type="password" name="mat_khau_khach_hang" value="<?php echo $row['mat_khau_khach_hang'] ?>">
                            </div>
                            <div class="form-group col-md-12">
                                Địa chỉ:
                                <input type="text" name="dia_chi_khach_hang" value="<?php echo $row['dia_chi_khach_hang'] ?>">
                            </div>
                           
                           <div class="form-group col-md-12">
                                Số điện thoại:
                                <input type="text" name="sdt_khach_hang" value="<?php echo $row['sdt_khach_hang'] ?>">
                            </div>
                            <div class="form-group col-md-6">
                                Ngày sinh
                                <input class="form-control" type="date" name="ngay_sinh_khach_hang" value="<?php echo $row['sdt_khach_hang'] ?>" >
                            </div>
                            <div  class="form-group col-md-6">
                                <div class="form-radio form-radio-inline">
                                <div class="form-radio-legend">Giới tính</div>
                                    <label class="form-radio-label">
                                        <input name="gioi_tinh_khach_hang" class="form-radio-field" type="radio" required checked value="1" />
                                        <i class="form-radio-button"></i>
                                        <span>Nam</span>
                                    </label>
                                    <label class="form-radio-label">
                                        <input name="gioi_tinh_khach_hang" class="form-radio-field" type="radio" required value="0" />
                                        <i class="form-radio-button"></i>
                                        <span>Nữ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <button name="button_submit" value="1" class="btn order_s_btn form-control">Chỉnh sửa thông tin</button>
                            </div>
                            
                        </form>
                    </div>
                    
                </div>
            </div>
        </section>