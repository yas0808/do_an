<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
</head>
<body>
	<?php
	session_start();
	if(empty($_SESSION)){
		header('location:dang_nhap_form.php?error_session');
	}
	$email_admin = $_SESSION['email_admin'];
	$mat_khau_admin = $_SESSION['mat_khau_admin'];
	$cap_do = $_SESSION['cap_do'];
	?>
	
	<h1><?php echo $ten_admin ?></h1>
	<h3><?php echo $email_admin ?></h3>
	<h3><?php echo $mat_khau_admin ?></h3>
	<h3><?php if($cap_do==1){
		echo "Cấp độ: User";
	}
	else if($cap_do==2){
		echo "Cấp độ: Admin";
	}
	else if($cap_do==3){
		echo "Cấp độ: Super Admin";
	} ?></h3>
	<a href="dang_xuat.php"><button>Đăng xuất</button></a>
</body>
</html>