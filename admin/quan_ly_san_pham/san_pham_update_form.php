<?php require_once('../kiem_tra_admin.php'); ?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin sửa sản phẩm</title>

        <!-- Icon css link -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet">
        <link href="../../vendors/linearicons/style.css" rel="stylesheet">
        <link href="../../vendors/flat-icon/flaticon.css" rel="stylesheet">
        <link href="../../vendors/stroke-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="../../vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="../../vendors/animate-css/animate.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="../../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="../../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">
        
        <link href="../../css/style.css" rel="stylesheet">
        <link href="../../css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!--================Main Header Area =================-->
        <header class="main_header_area">
            <div class="top_header_area row m0">
                <div class="container">
                    <div class="float-left">
                        <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                        <a href="mainto:halo888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> halo888@gmail.com</a>
                    </div>
                    
                    <div class="float-right">
                        <ul class="h_social list_style">
                            <li><a href="../../dang_xuat.php">Đăng xuất</a></li>
                            <li><a href="../../update_mat_khau.php">Đổi mật khẩu</a></li>
                        </ul>
                        <ul class="h_search list_style">
                            <li class="shop_cart"><a href="#"><i class="lnr lnr-cart"></i></a></li>
                            <li><a class="popup-with-zoom-anim" name="tim_kiem"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                       
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="my_toggle_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="dropdown submenu active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="trang_chu_admin.php" role="button" aria-haspopup="true" aria-expanded="false">Trang chủ</a>
                                </li>
                                <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="quan_ly_nha_san_xuat/nha_san_xuat_view.php">Xem nhà sản xuất</a></li>
                                        <li><a href="quan_ly_nha_san_xuat/nha_san_xuat_insert_form.php">Thêm nhà sản xuất</a></li>
                                    </ul>
                                    </li>
        
        
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý sản phẩm</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="san_pham_view.php">Xem sản phẩm</a></li>
                                        <li><a href="san_pham_insert_form.php">Thêm sản phẩm</a></li>
                                    </ul>
                                    </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                                    <ul class="dropdown-menu">
                                        
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php ">Hóa đơn chưa duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_duyet.php">Hóa đơn đã duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php">Hóa đơn chưa duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_huy.php ">Hóa đơn đã hủy</a></li>
                                    </ul>
                                    </li>
                                    <?php if($_SESSION['cap_do']==1){ ?>
                                      <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhân viên</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nhan_vien/nhan_vien_view.php">Tất cả nhân viên</a></li>
                                        <li><a href="../quan_ly_nhan_vien/insert_form_nhan_vien.php">Thêm nhân viên</a></li>
                                        <li><a href="../quan_ly_nhan_vien/update_form_nhan_vien.php">Chỉnh sửa thông tin nhân viên</a></li>
                                    </ul>
                                    </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_view.php">Tất cả nhà sản xuất</a></li>
                                        <li><a href="../quan_ly_nhan_vien/insert_form_nha_san_xuat.php">Thêm nhà sản xuất</a></li>
                                        <li><a href="../quan_ly_nhan_vien/update_form_nha_san_xuat.php">Chỉnh sửa thông tin nhà sản xuất</a></li>
                                    </ul>
                                    </li>
                                    <?php } ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
<body>
	<section class="contact_form_area p_100">
		<div class="container">
        		<div class="main_title">
					<h2>Sửa sản phẩm</h2>
					
				</div>
	<?php 
		
		require_once('../../ket_noi.php');
		//lấy toàn bộ nhà sản xuất để chọn
		$query_nxs = "select * from nha_san_xuat";
		$result_nxs = mysqli_query($connect,$query_nxs);

		//lấy thông tin sản phẩm muốn sửa
		$ma_san_pham = $_GET['ma_san_pham'];
		$query_sp = "select * from san_pham 
		where ma_san_pham = '$ma_san_pham'";
		$result_sp = mysqli_query($connect,$query_sp);
		$row_sp = mysqli_fetch_array($result_sp);
		mysqli_close($connect);
	?>
	<form class="row contact_us_form" action="san_pham_update_process.php" method="post" enctype="multipart/form-data">
		<input type="hidden" class="form-control" name="ma_san_pham" value="<?php echo $row_sp['ma_san_pham'] ?>">
		<div class="form-group col-md-6">
		<input type="text" class="form-control" name="ten_san_pham" value="<?php echo $row_sp['ten_san_pham'] ?>" >
		</div>
		<div class="form-group col-md-12">
			Giá
		<input type="number" class="form-control" name="gia" value="<?php echo $row_sp['gia'] ?>" style="width: 30%">
		</div>
		<div class="form-group col-md-6">
			Ảnh cũ
		<img src="anh/<?php echo $row_sp['anh'] ?>">
		<input type="hidden" class="form-control" name="anh_cu" value="<?php echo $row_sp['anh'] ?>">
		</div>
		<div class="form-group col-md-12">
			Ảnh mới
		<input type="file" class="form-control" name="anh_moi" accept="image/*">
		</div>
		<div class="form-group col-md-12">
		Nhà sản xuất
		<select class="form-control" name="ma_nha_san_xuat" style="  width: 20%;">
			<?php 
				while ($row_nxs = mysqli_fetch_array($result_nxs)) {
			?>
			<option value="<?php echo $row_nxs['ma_nha_san_xuat'] ?>"
			<?php 
				if($row_nxs['ma_nha_san_xuat']==$row_sp['ma_nha_san_xuat']){
					echo "selected";
				}
			?>
			>
				<?php echo $row_nxs['ten_nha_san_xuat'] ?>
			</option>
			<?php 
				}
			?>
		</select>
		</div>
		<div class="form-group col-md-6">
		<button name="button_submit" value="1">Sửa sản phẩm</button>
	</div>
	</form>
</div>
</section>
		<script src="....//js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/popper.min.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="../../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="../../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <!-- Extra plugin js -->
        <script src="../../vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="../../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
        <script src="../../vendors/datetime-picker/js/moment.min.js"></script>
        <script src="../../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../../vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="../../vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="../../vendors/lightbox/simpleLightbox.min.js"></script>
        
        <script src="../../js/theme.js"></script>
</body>
</html>