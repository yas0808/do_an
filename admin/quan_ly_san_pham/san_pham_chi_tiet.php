<?php require_once('../kiem_tra_admin.php'); ?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Xem chi tiết sản phẩm</title>

        <!-- Icon css link -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet">
        <link href="../../vendors/linearicons/style.css" rel="stylesheet">
        <link href="../../vendors/flat-icon/flaticon.css" rel="stylesheet">
        <link href="../../vendors/stroke-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="../../vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="../../vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="../../vendors/animate-css/animate.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="../../vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="../../vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">
        
        <link href="../../css/style.css" rel="stylesheet">
        <link href="../../css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!--================Main Header Area =================-->
        <header class="main_header_area">
            <div class="top_header_area row m0">
                <div class="container">
                    <div class="float-left">
                        <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                        <a href="mainto:halo888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> halo888@gmail.com</a>
                    </div>
                    
                    <div class="float-right">
                        <ul class="h_social list_style">
                            <li><a href="../../dang_xuat.php">Đăng xuất</a></li>
                            <li><a href="../update_mat_khau.php">Đổi mật khẩu</a></li>
                        </ul>
                        <ul class="h_search list_style">
                            <li class="shop_cart"><a href="#"><i class="lnr lnr-cart"></i></a></li>
                            <li><a class="popup-with-zoom-anim" name="tim_kiem"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                       
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="my_toggle_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="dropdown submenu active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="trang_chu_admin.php" role="button" aria-haspopup="true" aria-expanded="false">Trang chủ</a>
                                </li>
                                <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý khách hàng</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_khach_hang/view_khach_hang.php">Xem tất cả khách hàng</a></li>
                                        
                                    </ul>
                                    </li>
                    
        
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý sản phẩm</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="san_pham_view.php">Xem sản phẩm</a></li>
                                        <li><a href="san_pham_insert_form.php">Thêm sản phẩm</a></li>
                                    </ul>
                                    </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý hóa đơn</a>
                                    <ul class="dropdown-menu">
                                        
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php ">Hóa đơn chưa duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_duyet.php">Hóa đơn đã duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_chua_duyet.php">Hóa đơn chưa duyệt</a></li>
                                        <li><a href="../quan_ly_hoa_don/hoa_don_da_huy.php ">Hóa đơn đã hủy</a></li>
                                    </ul>
                                    </li>
                                    <?php if($_SESSION['cap_do']==1){ ?>
                                      <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhân viên</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nhan_vien/nhan_vien_view.php">Tất cả nhân viên</a></li>
                                        <li><a href="../quan_ly_nhan_vien/insert_form_nhan_vien.php">Thêm nhân viên</a></li>
                                        <li><a href="../quan_ly_nhan_vien/update_form_nhan_vien.php">Chỉnh sửa thông tin nhân viên</a></li>
                                    </ul>
                                    </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quản lý nhà sản xuất</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../quan_ly_nha_san_xuat/nha_san_xuat_view.php">Tất cả nhà sản xuất</a></li>
                                        <li><a href="../quan_ly_nhan_vien/insert_form_nha_san_xuat.php">Thêm nhà sản xuất</a></li>
                                        <li><a href="../quan_ly_nhan_vien/update_form_nha_san_xuat.php">Chỉnh sửa thông tin nhà sản xuất</a></li>
                                    </ul>
                                    </li>
                                    <?php } ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <section class="special_area p_100">
            <?php
        
        require_once('../../ket_noi.php');
        $tim_kiem = "";
        if(isset($_GET['tim_kiem'])){
            $tim_kiem = $_GET['tim_kiem'];
        }
        $page = 1;
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }
        //giới hạn sản phẩm của 1 trang
        $limit = 3;

        //bỏ qua bao nhiêu sản phẩm
        $offset = ($page-1)*$limit;

        //lấy sản phẩm để hiển thị trên 1 trang
        $query_show = "select * from san_pham
        join nha_san_xuat
        on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat
        where ten_san_pham like '%$tim_kiem%'
        limit $limit offset $offset";
        $result_show = mysqli_query($connect,$query_show);

        $query_count = "select count(*) from san_pham
        where ten_san_pham like '%$tim_kiem%'";
        $result_count = mysqli_query($connect,$query_count);
        $row_count    = mysqli_fetch_array($result_count);
        $count        = $row_count['count(*)'];

        $total_page = ceil($count/$limit);
        mysqli_close($connect);
    ?>
            <div class="container">
                <caption>
                <form>
                    Tìm kiếm: 
                    <input type="text" name="tim_kiem" value="<?php echo $tim_kiem ?>">
                    <button>Tìm</button>
                </form>
            </caption>
                <div class="special_item_inner">
                    <?php
                while($row = mysqli_fetch_array($result_show)){
            ?>
            
                    <div class="specail_item">
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-8">
                                <div class="special_item_text">
                                    <h4><?php echo $row['ten_san_pham'] ?></h4>
                                    <p><?php echo $row['mo_ta'] ?></p>
                                    <a class="pink_btn" href="../../khach_hang/them_vao_gio_hang.php"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="specail_item">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="s_item_left">
                                    <div class="main_title">
                                        <h2>Chi tiết sản phẩm</h2>
                                    </div>
                                    <ul class="list_style">
                                        <li><a href="#"><?php echo $row['ten_san_pham'] ?></a></li>
                                        <li><a href="#"><?php echo $row['nha_san_xuat'] ?></a></li>
                                        <li><a href="#"><?php echo $row['ma_nha_san_xuat'] ?></a></li>
                                        <li><a href="#"><?php echo $row['ma_loai_san_pham'] ?></a></li>
                                        <li><a href="#"><?php echo $row['gia']?></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="s_right_img">
                                    <img class="img-fluid" src="anh/<?php echo $row['anh'] ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </section>
<script src="....//js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/popper.min.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="../../vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="../../vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="../../vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <!-- Extra plugin js -->
        <script src="../../vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="../../vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
        <script src="../../vendors/datetime-picker/js/moment.min.js"></script>
        <script src="../../vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../../vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="../../vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="../../vendors/lightbox/simpleLightbox.min.js"></script>
        
        <script src="../../js/theme.js"></script>
</body>
</html>